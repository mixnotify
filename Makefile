CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -g
LDFLAGS = -lasound

TARGET = mixnotify

OBJS = mixnotify.o

all: $(TARGET)

$(TARGET): $(OBJS)
	cc ${LDFLAGS} -o $@ $(OBJS)

%.o: %.c
	cc $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

clean:
	-rm -f *.o *.d $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: clean
